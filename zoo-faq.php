<?php
/**
 * Plugin Name:     Zoo Faq
 * Plugin URI:      https://zoo.nl
 * Description:     clean and simple faq plugin
 * Author:          Ronnie Stevens
 * Author URI:      ronniestevens.co
 * Text Domain:     zoo-faq
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Zoo_Faq
 */

// Your code starts here.
require_once('post-types/faq.php');


function add_zoofaq_scripts() {
    wp_enqueue_style('css', plugins_url('/zoo-faq/style.css'), array(), '1.1', 'all');
    wp_enqueue_script('zoofaq-script', plugins_url('/zoo-faq/js/zoofaq.js'), array( 'jquery' ), 1.2, true);
  }
  add_action( 'wp_enqueue_scripts', 'add_zoofaq_scripts' );
  