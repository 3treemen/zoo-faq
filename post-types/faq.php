<?php

/**
 * Registers the `faq` post type.
 */
function faq_init() {
	register_post_type( 'faq', array(
		'labels'                => array(
			'name'                  => __( 'FAQs', 'zoo-faq' ),
			'singular_name'         => __( 'FAQ', 'zoo-faq' ),
			'all_items'             => __( 'All FAQs', 'zoo-faq' ),
			'archives'              => __( 'FAQ Archives', 'zoo-faq' ),
			'attributes'            => __( 'FAQ Attributes', 'zoo-faq' ),
			'insert_into_item'      => __( 'Insert into FAQ', 'zoo-faq' ),
			'uploaded_to_this_item' => __( 'Uploaded to this FAQ', 'zoo-faq' ),
			'featured_image'        => _x( 'Featured Image', 'faq', 'zoo-faq' ),
			'set_featured_image'    => _x( 'Set featured image', 'faq', 'zoo-faq' ),
			'remove_featured_image' => _x( 'Remove featured image', 'faq', 'zoo-faq' ),
			'use_featured_image'    => _x( 'Use as featured image', 'faq', 'zoo-faq' ),
			'filter_items_list'     => __( 'Filter FAQs list', 'zoo-faq' ),
			'items_list_navigation' => __( 'FAQs list navigation', 'zoo-faq' ),
			'items_list'            => __( 'FAQs list', 'zoo-faq' ),
			'new_item'              => __( 'New FAQ', 'zoo-faq' ),
			'add_new'               => __( 'Add New', 'zoo-faq' ),
			'add_new_item'          => __( 'Add New FAQ', 'zoo-faq' ),
			'edit_item'             => __( 'Edit FAQ', 'zoo-faq' ),
			'view_item'             => __( 'View FAQ', 'zoo-faq' ),
			'view_items'            => __( 'View FAQs', 'zoo-faq' ),
			'search_items'          => __( 'Search FAQs', 'zoo-faq' ),
			'not_found'             => __( 'No FAQs found', 'zoo-faq' ),
			'not_found_in_trash'    => __( 'No FAQs found in trash', 'zoo-faq' ),
			'parent_item_colon'     => __( 'Parent FAQ:', 'zoo-faq' ),
			'menu_name'             => __( 'FAQs', 'zoo-faq' ),
		),
		'public'                => true,
		'hierarchical'          => false,
		'show_ui'               => true,
		'show_in_nav_menus'     => false,
		'supports'              => array( 'title', 'excerpt','page-attributes','thumbnail' ),
		'has_archive'           => true,
		'rewrite'               => true,
		'query_var'             => true,
		'menu_position'         => null,
		'menu_icon'             => 'dashicons-admin-post',
		'show_in_rest'          => true,
		'rest_base'             => 'faq',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'faq_init' );

/**
 * Sets the post updated messages for the `faq` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `faq` post type.
 */
function faq_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['faq'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'FAQ updated. <a target="_blank" href="%s">View FAQ</a>', 'zoo-faq' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'zoo-faq' ),
		3  => __( 'Custom field deleted.', 'zoo-faq' ),
		4  => __( 'FAQ updated.', 'zoo-faq' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'FAQ restored to revision from %s', 'zoo-faq' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		/* translators: %s: post permalink */
		6  => sprintf( __( 'FAQ published. <a href="%s">View FAQ</a>', 'zoo-faq' ), esc_url( $permalink ) ),
		7  => __( 'FAQ saved.', 'zoo-faq' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'FAQ submitted. <a target="_blank" href="%s">Preview FAQ</a>', 'zoo-faq' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'FAQ scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview FAQ</a>', 'zoo-faq' ),
		date_i18n( __( 'M j, Y @ G:i', 'zoo-faq' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'FAQ draft updated. <a target="_blank" href="%s">Preview FAQ</a>', 'zoo-faq' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'faq_updated_messages' );


add_action( 'add_meta_boxes', array ( 'T5_Richtext_Excerpt', 'switch_boxes' ) );

/**
 * Replaces the default excerpt editor with TinyMCE.
 */
class T5_Richtext_Excerpt
{
    /**
     * Replaces the meta boxes.
     *
     * @return void
     */
    public static function switch_boxes()
    {
        if ( ! post_type_supports( $GLOBALS['post']->post_type, 'excerpt' ) )
        {
            return;
        }

        remove_meta_box(
            'postexcerpt' // ID
        ,   ''            // Screen, empty to support all post types
        ,   'normal'      // Context
        );

        add_meta_box(
            'postexcerpt2'     // Reusing just 'postexcerpt' doesn't work.
        ,   __( 'Excerpt' )    // Title
        ,   array ( __CLASS__, 'show' ) // Display function
        ,   null              // Screen, we use all screens with meta boxes.
        ,   'normal'          // Context
        ,   'core'            // Priority
        );
    }

    /**
     * Output for the meta box.
     *
     * @param  object $post
     * @return void
     */
    public static function show( $post )
    {
    ?>
        <label class="screen-reader-text" for="excerpt"><?php
        _e( 'Excerpt' )
        ?></label>
        <?php
        // We use the default name, 'excerpt', so we don’t have to care about
        // saving, other filters etc.
        wp_editor(
            self::unescape( $post->post_excerpt ),
            'excerpt',
            array (
            'textarea_rows' => 15
        ,   'media_buttons' => FALSE
        ,   'teeny'         => TRUE
        ,   'tinymce'       => TRUE
            )
        );
    }

    /**
     * The excerpt is escaped usually. This breaks the HTML editor.
     *
     * @param  string $str
     * @return string
     */
    public static function unescape( $str )
    {
        return str_replace(
            array ( '&lt;', '&gt;', '&quot;', '&amp;', '&nbsp;', '&amp;nbsp;' )
        ,   array ( '<',    '>',    '"',      '&',     ' ', ' ' )
        ,   $str
        );
    }
}

/**
 * Taxonomy
 */
//hook into the init action and call create_categories_nonhierarchical_taxonomy when it fires
 
add_action( 'init', 'create_categories_nonhierarchical_taxonomy', 0 );
 
function create_categories_nonhierarchical_taxonomy() {
 
// Labels part for the GUI
 
  $labels = array(
    'name' => _x( 'Categories', 'taxonomy general name' ),
    'singular_name' => _x( 'Category', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Categories' ),
    'popular_items' => __( 'Popular Categories' ),
    'all_items' => __( 'All Categories' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit Category' ), 
    'update_item' => __( 'Update Category' ),
    'add_new_item' => __( 'Add New Category' ),
    'new_item_name' => __( 'New Category Name' ),
    'separate_items_with_commas' => __( 'Separate categories with commas' ),
    'add_or_remove_items' => __( 'Add or remove categories' ),
    'choose_from_most_used' => __( 'Choose from the most used categories' ),
    'menu_name' => __( 'Categories' ),
  ); 
 
// Now register the non-hierarchical taxonomy like tag
 
  register_taxonomy('categories','faq',array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'show_in_rest' => true,
    'show_admin_column' => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var' => true,
    'rewrite' => array( 'slug' => 'categories' ),
  ));
}

function zoo_faqs ($atts) {
	$atts = shortcode_atts( array(
		'category' => '', 
    'class' => '',
	), $atts);

	extract( $atts );

	$args = array(
		'post_type' => 'faq',
        'categories' => $category,
        'orderby' => 'menu_order',
        'order' => 'ASC',
	);

	$content = '';
	
	$query = new WP_Query( $args );
	if( $query->have_posts() ) {
  	$content .= '<div class="faqs ' .esc_attr($atts['class']). '">';
		while ( $query->have_posts() ) {
			$query->the_post();
			$title = get_the_title();
      $img_url = get_the_post_thumbnail_url($post->ID, 'medium');
			$content .= '<div class="faq" ';
      if($img_url) {
        $content .= 'style="background-image:url(' . $img_url . ')"';
      }

      $content .= ' >';
			$content .= '<div class="question"><div>' . $title . '</div>';
      if(!$img_url) {
        $content .= '<span class="plus">&plus;</span>';
      }
      $content .= '</div>';
			$content .= '<div class="answer">' . get_the_excerpt() . '</div>';
			$content .= '</div>';
		}
		$content .= '</div>';
		wp_reset_postdata();
	}
	return $content;
}
add_shortcode( 'zoofaqs', 'zoo_faqs' );







/**
* add order column to admin listing screen for header text
*/
function add_order_column($page_columns) {
    $page_columns['menu_order'] = "Order";
    return $page_columns;
}
add_action('manage_faq_posts_columns', 'add_order_column');

  /**
* show custom order column values
*/
function show_order_column($name){
    global $post;
  
    switch ($name) {
      case 'menu_order':
        $order = $post->menu_order;
        echo $order;
        break;
     default:
        break;
     }
  }
  add_action('manage_faq_posts_custom_column','show_order_column');